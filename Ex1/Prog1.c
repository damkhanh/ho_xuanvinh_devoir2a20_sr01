#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
int main(int argc, char *argv[], char *envp[])
{
    pid_t fils1, fils2, fils3, fils4;   
    int etat;
    // int i=0;
    fils1 = fork();
    if (fils1 == 0)
    {
        // printf("%d fils1\n", i++);
        printf("Mon pid est : %d et le pid de mon père est : %d\n",getpid(), getppid());
    }
    else wait(&etat);

    fils2 = fork();
    if (fils2 == 0)
    {
        // printf("%d fils2\n", i++);
        printf("Mon pid est : %d et le pid de mon père est : %d\n", getpid(), getppid());
    }
    else wait(&etat);

    fils3 = fork();
    if (fils3 == 0)
    {
        // printf("%d fils3\n", i++);
        printf("Mon pid est : %d et le pid de mon père est : %d\n", getpid(), getppid());
    }
    else wait(&etat);

    fils4 = fork();
    if (fils4 == 0)
    {
        // printf("%d fils4\n", i++);
        printf("Mon pid est : %d et le pid de mon père est : %d\n", getpid(), getppid());
    }
    else wait(&etat);
    return 0;
}