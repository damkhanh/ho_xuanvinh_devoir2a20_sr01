#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include "ApplicationManager.h"

int main(int argc, char *argv[], char *envp[])
{
    char filename[50];
    printf("Charger un  fichier\n");
    printf("Quel est le nom de le fichier? \n");
    scanf("%s", filename);
    printf("\n");
    ApplicationManager(filename);
    return 0;
}