#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void main(int argc, char *argv[])
{

	FILE *fp;
	char c;
	while (1)
	{
		printf("[power manager]: running\n");
		if (argc != 4)
		{
			exit(EXIT_FAILURE);
		}
		fp = fopen(argv[1], "r");
		if (fp == NULL)
		{
			exit(EXIT_FAILURE);
		}
		c = fgetc(fp);
		fclose(fp);
		if (c == '1')
		{
			printf("[power manager] Mise en veille en cours ...\n");
			/* ajoutez vos modification ici */
				pid_t main_process;
				main_process = atoi(argv[3]);
				// printf("PID of main process is : |%d|\n",main_process);
				kill(main_process, SIGUSR1);
			fp = fopen(argv[1], "w");
			fputs("0", fp);
			fclose(fp);
		}
		sleep(atoi(argv[2]));
	}
}
