#ifndef SR01_Devoir2_Ex3_H
#define SR01_Devoir2_Ex3_H
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>


void ding(int sig);

char *cast(char *source, int start, int end);
char *prolong(char *des, char *src);
char *create_call(int nombre_de_argument, char **declaration, char *path);
void ApplicationManager(char *filename);

#endif //SR01_Devoir2_Ex3_H