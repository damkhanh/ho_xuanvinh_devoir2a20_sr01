#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

void main(int argc, char *argv[])
{
    int sec=0, h, m, s;

    while (1)
    {
        h = (sec / 3600);
        m = (sec - (3600 * h)) / 60;
        s = (sec - (3600 * h) - (m * 60));
        printf("[time manager]: running time is H:M:S - %d:%d:%d\n", h, m, s);
        sec+=5;
        sleep(5);
    }
}
