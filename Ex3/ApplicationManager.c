#include "ApplicationManager.h"
#define MAX 100

static pid_t child_process;
char *cast(char *source, int start, int end)
{
    char *des = malloc(sizeof(char) * (end - start + 2));
    for (int i = start; i < end; i++)
    {
        des[i - start] = source[i];
    }
    des[end - start + 2] = '\0';
    return des;
}

char *prolong(char *des, char *src)
{
    char *temp = malloc(sizeof(char) * (strlen(des) + strlen(src) + 2));
    char *m = malloc(sizeof(char) * 2);
    m[0] = ' ';
    m[1] = '\0';
    strcpy(temp, des);
    strcpy(temp + strlen(des), m);
    strcpy(temp + strlen(des) + 1, src);
    return temp;
}
char *create_call(int nombre_de_argument, char **declaration, char *path)
{
    // printf("I am herre\n");
    if (nombre_de_argument == 0)
    {
        // printf("%d\n",nombre_de_argument);
        char *res = malloc(sizeof(char) * strlen(path));
        strcpy(res, path);
        //printf("res= |%s|\n", res);
        return res;
    }
    else
    {
        //printf("nombre de argument >0 est = %d\n", nombre_de_argument);
        int length_total = strlen(path) + 1;
        for (int i = 0; i < nombre_de_argument; i++)
        {
            length_total += strlen(declaration[i]) + 1;
        }
        char *res = malloc(sizeof(char) * length_total);
        res = strcpy(res, path);
        for (int i = 0; i < nombre_de_argument; i++)
        {
            res = prolong(res, declaration[i]);
            //printf("res = |%s|\n", res);
        }
        //printf("res = |%s|\n", res);
        return res;
    }
}

void finish_application(int sig)
{
    printf("\n\n|--Finish Application Manager--|\n\n");
    sleep(0.5);
    killpg(child_process, SIGTERM);
}

void ApplicationManager(char *filename)
{
    FILE *flist;
    pid_t wpid;
    int errnum;
    int nombre_de_application = 0;
    flist = fopen(filename, "r");
    if (flist == NULL)
    {
        errnum = errno;
        fprintf(stderr, "Value of errno: %d\n", errno);
        perror("Error printed by perror");
        fprintf(stderr, "Error opening file: %s\n", strerror(errnum));
    }
    else
    {
        char title[MAX];
        FILE *filePointer;
        fgets(title, 100, flist);

        //printf("|%s|\n",title);
        //printf("%c\n",title[strlen(title)-3]);

        nombre_de_application = title[strlen(title) - 3] - 48;
        if (nombre_de_application == 0)
        {
            printf("On a pas aucune applications dans cette liste\n");
            fclose(flist);
            exit(EXIT_SUCCESS);
        }
        printf("Nombre de application = %d\n", nombre_de_application);

        //Start to create information about applications
        char *name[nombre_de_application];
        char *path[nombre_de_application];
        int nombre_de_argument[nombre_de_application];
        char *declaration[nombre_de_application][MAX];
        for (int i = 0; i < nombre_de_application; i++)
        {
            //get name of application
            fgets(title, 100, flist);
            //printf("Title here is |%s|\n", title);
            name[i] = cast(title, 5, strlen(title) - 2);
            //printf("Name %d est |%s|\n", i + 1, name[i]);
            //get path of application
            fgets(title, 100, flist);
            path[i] = cast(title, 5, strlen(title) - 2);
            //printf("Path %d est |%s|\n", i + 1, path[i]);
            //get number of arguments
            fgets(title, 100, flist);
            nombre_de_argument[i] = title[strlen(title) - 3] - 48;
            //printf("Nombre de argument here = %d\n", nombre_de_argument[i]);
            if (nombre_de_argument[i] == 0)
            {
                //pass 2 ligne
                fgets(title, 100, flist);
                fgets(title, 100, flist);
            }
            else
            {
                //printf("Nombre de argument > 0\n");
                fgets(title, 100, flist);
                for (int k = 0; k < nombre_de_argument[i]; k++)
                {
                    //printf("create path \n");
                    fgets(title, 100, flist);
                    declaration[i][k] = cast(title, 0, strlen(title) - 2);
                    //printf("element %d  = %s \n", k, declaration[i][k]);
                }
                fgets(title, 100, flist);
            }
        }
        fclose(flist);
        printf("\n\n|--Application Manager: \n\n");
        pid_t pid1 = getpid(), pid2;
        // printf("Pid de parent est: |%d|\n", pid1);
        for (int i = 0; i < nombre_de_application; i++)
        {
            pid2 = fork();
            if (pid2 == -1)
            {
                perror("Erreur de fork()");
            }
            else if (pid2 == 0)
            {
                // printf("Testtttt\n");
                // sleep(20);
                printf("|--Start Application: %s\n", name[i]);
                if (nombre_de_argument[i] == 0)
                {
                    char *chaine = path[i];
                    // char *args[] = {"bash", "-c", chaine, NULL};
                    // execv("/bin/bash", args);
                    system(chaine);
                }
                else
                {
                    child_process = getpgid(pid2);
                    // printf("Get PID = |%d| and child_process = |%d|\n", getpid(), child_process);
                    char *chaine = create_call(nombre_de_argument[i], declaration[i], path[i]);
                    char extra[10];
                    sprintf(extra, "%d", pid1);
                    //printf("Extra = |%s|\n",extra);
                    chaine = prolong(chaine, extra);
                    // char *args[] = {"bash", "-c", chaine, NULL};
                    // execv("/bin/bash", args);
                    //printf("Chaine used is |%s|\n",chaine1);
                    system(chaine);
                }
                printf("\n\n|--Application %s est fini--|\n\n", name[i]);
                sleep(2);
                exit(0);
            }
        }
        // printf("PID2 = |%d|\n", pid2);
        int etat = 0;
        signal(SIGUSR1, finish_application);
        while ((wpid = wait(&etat)) > 0)
            ;
    }
}
