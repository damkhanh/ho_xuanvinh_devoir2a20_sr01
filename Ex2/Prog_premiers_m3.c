//Ex2_Partie3_3
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

void attendre(int signum)
{
    wait(NULL);
}

void my_system(char *chaine)
{
    int pid;
    pid = fork();
    if (pid == -1)
    {
        perror("Erreur de fork()");
    }
    else if (pid == 0)
    {
        char *args[] = {"bash", "-c", chaine, NULL};
        execv("/bin/bash", args);
    }
    else
    {
        signal(SIGCHLD, attendre);
    }
    
}

int premier(int nb)
{
    int r = 0;
    for (int i = 1; i <= nb; i++)
    {
        if (nb % i == 0)
            r++;
    }
    if (r > 2)
        return 0;
    else
        return 1;
}
void explorer(int debut, int fin)
{
    int etat, pid, pid2;
    pid = fork();
    if (pid == 0)
    {
        for (int i = debut; i <= fin; i++)
        {
            if (premier(i) == 1) // i est un premier
            {
                pid2 = fork();
                char chaine[300];
                sprintf(chaine, "echo -e ' %d est nombre premier \n|--PID du processus exécute la boucle for : %d \n|--PID du processus créé le processus system : %d \n|--PID du processus écrit le message sur le terminal : ' $$>>nbr_premiers.txt", i, getppid(), getpid());
                if (pid2 == 0)
                {
                    my_system(chaine);
                    sleep(2);
                    exit(0);
                }
                else
                {
                    signal(SIGCHLD, attendre);
                }
            }
        }
        exit(0);
    }
    else
    {
        signal(SIGCHLD, attendre);
    }
}
int main()
{
    int grp = 1;
    while (grp <= 11)
    {
        explorer(grp + 1, grp + 10);
        grp = grp + 10;
    }
}