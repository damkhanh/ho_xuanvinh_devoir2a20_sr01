#include "my_system.h"

void my_system(char *chaine)
{
    int pid;
    pid = fork();
    if (pid == -1){
        perror("Erreur de fork()");
    }
    else if (pid == 0)
    {
        char *args[] = {"bash", "-c", chaine, NULL};
        execv("/bin/bash", args);
    }
}