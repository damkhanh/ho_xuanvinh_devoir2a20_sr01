#include "explorer.h"
#include "my_system.h"
#include "premier.h"

void explorer(int debut, int fin)
{
    int etat, pid, pid2;
    pid = fork();
    if (pid == 0)
    {
        for (int i = debut; i <= fin; i++)
        {
            if (premier(i) == 1) // i est un premier
            {
                pid2 = fork();
                char chaine[300];
                //sprintf(chaine,"echo -e '%d %d %d' $$>>nbr_premiers.txt",i,getppid(),getpid());
                sprintf(chaine, "echo -e ' %d est nombre premier \n|--PID du processus exécute la boucle for : %d \n|--PID du processus créé le processus system : %d \n|--PID du processus écrit le message sur le terminal : ' $$>>nbr_premiers.txt", i, getppid(), getpid());
                if (pid2 == 0)
                {
                    // (chaine,"echo -e '%d %d %d' $$>>nbr_premiers.txt",i,getppid(),getpid());
                    my_system(chaine);
                    sleep(2);
                    exit(0);
                }
                else
                    wait(&etat); // instruction 41
            }
        }
        exit(0);
    }
    else
        wait(&etat); // instruction 46
}