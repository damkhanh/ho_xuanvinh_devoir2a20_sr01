//Ex2_Partie3_1
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

int premier(int nb)
{
    int r = 0;
    for (int i = 1; i <= nb; i++)
    {
        if (nb % i == 0)
            r++;
    }
    if (r > 2)
        return 0;
    else
        return 1;
}
void explorer(int debut, int fin)
{
    int etat, pid, pid2;
    pid = fork();
    if (pid == 0)
    {
        for (int i = debut; i <= fin; i++)
        {
            if (premier(i) == 1) // i est un premier
            {
                pid2 = fork();
                if (pid2 == 0)
                {
                    char chaine1[200];
                    char chaine2[200];
                    sprintf(chaine1, "echo ' %d nombre premier | PID du processus écrit le message sur le terminal : ' $$>>nbr_premiers.txt", i);
                    int ret = system(chaine1);

                    if (WIFEXITED(ret) && !WEXITSTATUS(ret))
                    {
                        printf("Completed successfully\n"); ///successful
                        sprintf(chaine2,"echo 'PID du processus exécute la boucle for : %d | PID du processus créé le processus system : %d'>>nbr_premiers.txt",getppid(), getpid());
                        system(chaine2);
                    }
                    else
                    {
                        printf("Execution failed\n"); //error
                    }
                    sleep(2);
                    exit(0);
                }
                else
                    wait(&etat); // instruction 41
            }
        }
        exit(0);
    }
    else
        wait(&etat); // instruction 46
}
int main()
{
    int grp = 1;
    while (grp <= 11)
    {
        explorer(grp + 1, grp + 10);
        grp = grp + 10;
    }
}