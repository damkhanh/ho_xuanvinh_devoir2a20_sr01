#ifndef SR01_Devoir2_Explorer_H
#define SR01_Devoir2_Explorer_H
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>

void explorer(int debut, int fin);

#endif //SR01_Devoir2_Explorer_H